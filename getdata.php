<?php

$memcache = new Memcached();
$memcache->addServer('localhost', 11211);

$round = 0;
while (1) {
	echo date("Y-m-d H:i:s"). "\n=============================================================================\n";
	echo date("Y-m-d H:i:s"). ": LOOP Started\n";
	$round++;
	$altcoin_data = file_get_contents('https://www.worldcoinindex.com/apiservice/ticker?key=6Hc4tXdNuKbrnVmlihNXBT10rVcs5K&label=cpsbtc&fiat=btc');
	$bitpay_data = file_get_contents('https://bitpay.com/api/rates');
	$altcoin_rates = json_decode($altcoin_data, true);
	$bitpay_rates = json_decode($bitpay_data, true);
	$cps_rates = [];

	if(is_array($altcoin_rates) && !isset($altcoin_rates["error"]) && count($altcoin_rates["Markets"]) > 0){
		foreach ($altcoin_rates["Markets"] as $market) {
			$code = explode("/", $market["Label"])[0];
			$name = $market["Name"];
			$price = round($market["Price"], 8);
			foreach($bitpay_rates as $bitpay_rate){
				$bitpay_rate["rate"] = $bitpay_rate["rate"] * $price;
				$key = array("code" => $bitpay_rate["code"], "name" => $bitpay_rate["name"], "rate" => round($bitpay_rate["rate"], 8));
				array_push($cps_rates, $key);
			}
			$key = array("code" => $code,"name" => $name, "rate" => 1);
			array_push($cps_rates, $key);
		}
		$memcache->set("rates", $cps_rates);
	} else {
		echo date("Y-m-d H:i:s"). ": API is Down, retry in 60 secs\n";
	}

	$data = file_get_contents('https://min-api.cryptocompare.com/data/pricemulti?fsyms=GRS,BTC,ETH,XTZ,EOS,XMR,XRP,XLM,STEEM,PART,DCR,BCH,ZEC,CPC&tsyms=BTC,USD,EUR,GBP,KRW,JPY,CAD,AUD,CNY,CHF,PKR,INR,AED');
	$rates = json_decode($data, true);
	$coinnames = array(
		"BTC" => "Bitcoin",
		"GRS" => "Groestlcoin",
		"ETH" => "Ethereum",
		"XTZ" => "Tezos",
		"EOS" => "EOS",
		"XMR" => "Monero",
		"XRP" => "Ripple",
		"XLM" => "Stellar",
		"STEEM" => "Steemit",
		"PART" => "Particl",
		"DCR" => "Decred",
		"BCH" => "Bcash",
		"CPC" => "Capricoin",
		"ZEC" => "Zcash"
	);

	if(count($rates) > 0) {
		foreach($rates as $key => $rate) {
			$DynamicCoinName = strtolower($key)."_rates";
			$$DynamicCoinName = array(
			array("code" => "$key","name" => $coinnames["$key"],				"rate" => 1),
			array("code" => "BTC","name" => "Bitcoin",				"rate" => $rate["BTC"]),
			array("code" => "USD","name" => "US Dollar",     		"rate" => $rate["USD"]),
			array("code" => "EUR","name" => "Eurozone Euro", 		"rate" => $rate["EUR"]),
			array("code" => "GBP","name" => "Pound Sterling",		"rate" => $rate["GBP"]),
			array("code" => "KRW","name" => "Korean Won", 			"rate" => $rate["KRW"]),
			array("code" => "JPY","name" => "Japanese  Yen", 		"rate" => $rate["JPY"]),
			array("code" => "CAD","name" => "Canadian Dollar",		"rate" => $rate["CAD"]),
			array("code" => "AUD","name" => "Australian Dollar", 	"rate" => $rate["AUD"]),
			array("code" => "CNY","name" => "Chinese Yuan", 		"rate" => $rate["CNY"]),
			array("code" => "CHF","name" => "Swiss Franc", 			"rate" => $rate["CHF"]),
			array("code" => "AED","name" => "UAE Dirham", 			"rate" => $rate["AED"]),
			array("code" => "PKR","name" => "Pakistani Rupee", 		"rate" => $rate["PKR"]),
			array("code" => "INR","name" => "Indian Rupee", 		"rate" => $rate["INR"])
			);
			$memcache->set("$DynamicCoinName", $$DynamicCoinName);
		}
	} else {
		echo date("Y-m-d H:i:s"). ": API is Down, retry in 60 secs\n";
	}			
	echo date("Y-m-d H:i:s"). ": LOOP DONE\n";
	echo date("Y-m-d H:i:s"). ": Round: " . $round. "\n";
	echo date("Y-m-d H:i:s"). ": Sleeping...\n";          
	echo "=============================================================================\n";
	sleep(60);
}
?>