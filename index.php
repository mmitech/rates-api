<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
$jsonquery = file_get_contents('php://input');
$json = json_decode($jsonquery, true);

$memcache = new Memcached();
$memcache->addServer('localhost', 11211);

// we correct the request data
function mysql_fix_escape_string($text){
    if(is_array($text)) 
        return array_map(__METHOD__, $text); 
    if(!empty($text) && is_string($text)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), 
                           array('', '', '', '', "", '', ''),$text); 
    } 
    $text = str_replace("'","",$text);
    $text = str_replace('"',"",$text);
    return $text;
}

$api = $_GET['api'];
$api = mysql_fix_escape_string($api);
$coinRate = $memcache->get($api);
if ($memcache->getResultCode() == Memcached::RES_SUCCESS) {
    $coinRate = json_encode($coinRate);
	print_r($coinRate);
} else {
    $rates = $memcache->get("rates");
    if ($memcache->getResultCode() == Memcached::RES_SUCCESS) {
        $rates = json_encode($rates);
        print_r($rates);
    } else {
        print_r(json_encode(array(
            "code" => 404,
            "status" => false,
            "error" => "We can't find what you are looking for",
            "server_time" => date("Y-m-d H:i:s")
        )));
    }
}
?>
